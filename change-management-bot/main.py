#main of discord bot

import discord
from gsheet import *

client = discord.Client()
sheet = gsheet()

@client.event
async def on_ready():
    print('We have logged in as {0.user}'.format(client))

@client.event
async def on_message(message):
    if message.author == client.user:
        return

    # Help command
    if message.content.startswith('!help'):
        embed=discord.Embed(title="Commands", description="View all the different commands and how to use them", color=0xFF5733)
        embed.add_field(name = "Incident response", value= "!ir <Target(s)>, <Time(s)>, <Technique(s)>, <Information/Acess>, <Gained Source of Attack>")
        await message.channel.send(embed=embed)

    # Incident Response Command to insert data to excel
    if message.content.startswith('!ir '):
        SPREADSHEET_ID = os.environ['SPREADSHEET_ID'] # Google sheets ID
        RANGE_NAME = 'A1' #Where to start
        FIELDS = 5 # Amount of fields/cells
    
        # Code
        msg = message.content[3:]
        result = [x.strip() for x in msg.split(',')]
        if len(result) == FIELDS:
            # Add
            print(message.created_at)
            DATA = [message.author.name] + [str(message.author.id)] + [str(message.created_at)] + result
            sheet.add(SPREADSHEET_ID, RANGE_NAME, DATA)
            await message.channel.send('Incident Response data has been successfully submitted!')
        else:
            # Error message
            await message.channel.send('Error: You need to add {0} fields, meaning it can only have {1} comma.'.format(FIELDS,FIELDS-1))
    elif message.content.startswith('!cm '):
        SPREADSHEET_ID = os.environ['SPREADSHEET_ID'] # Google sheets ID
        RANGE_NAME = 'A1' #Where to start
        FIELDS = 2 # Amount of fields/cells

        # Code
        msg = message.content[3:]
        result = [x.strip() for x in msg.split(',')]
        if len(result) == FIELDS:
            # Add
            print(message.created_at)
            DATA = [message.author.name] + [str(message.author.id)] + [str(message.created_at)] + result
            sheet.add(SPEADSHEET_ID, RANGE_NAME, DATA)
            await message.channel.send('Change Management data has been successfully submitted!')
        else:
            wait message.channel.send('Error: You need to add {0} fields, meaning it can only have {1} comma.'.format(FIELDS,FIELDS-1))

client.run(os.environ['TOKEN']) # Add bot token here