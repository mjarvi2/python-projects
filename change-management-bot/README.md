This bot was made by Team Japan for the 2022 Case Studies in Cyber Operations class

This is a bot for discord servers. This is made to document
small changes in systems. When connected to a discord server,
a user will enter use !cm <machine name>, <change description>
!ir <machine name>, <ir description> can also be used to take notes on 
incident response.
!help can also be used if a user does not know the commands of the bot